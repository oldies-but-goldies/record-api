# BEE Team - Backend Engineer Challenge
## Intro
At the beginning idea was to reuse some parts(DTO Models) generated code with designed first [swagger.yml]() located in project root. But default swagger generator results for Symfony look quite outdated.

## Installation/setup
### Git
Clone the repo
```
git clone https://gitlab.com/oldies-but-goldies/record-api.git
```

### Docker
All configuration is provided with `docker-compose.yml` and `./docker` folder for building **nginx** and **php-fpm** containers.

### Configuration
**Note:** There are two `.env` files required:
* In project root, used for docker configuration. There is `.env.dist` in repo as a template/list of expected vars. My recommendation/sample with values:
```dotenv
MYSQL_PASSWORD=123
MYSQL_ROOT_PASSWORD=12345
MYSQL_DATABASE=recordapi
MYSQL_USER=user
TZ=UTC
``` 
* In Symfony app root `./api`, used for Symfony configuration. There is `.env.dist` in repo as a template/list of expected vars. My recommendation/sample with values:
```dotenv
# This file is a "template" of which env vars need to be defined for your application
# Copy this file to .env file for development, create environment variables when deploying to production
# https://symfony.com/doc/current/best_practices/configuration.html#infrastructure-related-configuration

###> symfony/framework-bundle ###
APP_ENV=dev
APP_SECRET=oka2a9cxhkwuwhqifpm2maqez5v8c629
#TRUSTED_PROXIES=127.0.0.1,127.0.0.2
#TRUSTED_HOSTS=localhost,example.com
###< symfony/framework-bundle ###

###> lexik/jwt-authentication-bundle ###
# Key paths should be relative to the project directory
JWT_PRIVATE_KEY_PATH=config/jwt/private.pem
JWT_PUBLIC_KEY_PATH=config/jwt/public.pem
JWT_SECRET_KEY=config/jwt/private.pem
JWT_PUBLIC_KEY=config/jwt/public.pem
JWT_PASSPHRASE=55m25w8wmmimekfapjohyfrn7t9dc4yu
###< lexik/jwt-authentication-bundle ###

###> doctrine/doctrine-bundle ###
# Format described at http://docs.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/configuration.html#connecting-using-a-url
# For an SQLite database, use: "sqlite:///%kernel.project_dir%/var/data.db"
# Configure your db driver and server_version in config/packages/doctrine.yaml
DATABASE_URL=mysql://root:12345@db:3306/recordapi
###< doctrine/doctrine-bundle ###

###> symfony/swiftmailer-bundle ###
# For Gmail as a transport, use: "gmail://username:password@localhost"
# For a generic SMTP server, use: "smtp://localhost:25?encryption=&auth_mode="
# Delivery is disabled by default via "null://localhost"
MAILER_URL=null://localhost
###< symfony/swiftmailer-bundle ###

###> nelmio/cors-bundle ###
CORS_ALLOW_ORIGIN=^https?://localhost:?[0-9]*$
###< nelmio/cors-bundle ###

MYSQL_PASSWORD=123
MYSQL_ROOT_PASSWORD=12345
MYSQL_DATABASE=recordapi
MYSQL_USER=user
``` 

### Installation steps
#### Docker
**Note:** To make proper `.env` files from `.env.dist` according to samples above.

1. Building containers 
    ```
    docker-compose build
    ```
2. Starting containers
    ```
    docker-compose up -d
    ```
Expecting successfully running containers via command `docker-compose ps`, for example:
```
    Name                   Command               State                 Ports               
-------------------------------------------------------------------------------------------
records-api     docker-php-entrypoint php-fpm    Up      9000/tcp                          
records-db      docker-entrypoint.sh mysqld      Up      0.0.0.0:33061->3306/tcp, 33060/tcp
records-nginx   /docker-entrypoint.sh ngin ...   Up      0.0.0.0:8181->80/tcp  
```
#### Symfony deps installation
It can be triggered with command:
```
docker-compose exec api php -d memory_limit=-1 /usr/local/bin/composer install
```
#### Symfony migrations applying
**Note:** It's interactive, to type `y` is required.
```
docker-compose exec api php bin/console doctrine:migrations:migrate
```

#### Symfony fake data fixtures applying to db
**Note:** It's interactive, to type `yes` is required.
```
docker-compose exec api php bin/console doctrine:fixtures:load
```
Expected output:
```

 Careful, database "recordapi" will be purged. Do you want to continue? (yes/no) [no]:
 > yes 

   > purging database
   > loading App\DataFixtures\AlbumFixtures
   > loading App\DataFixtures\ArtistFixtures
   > loading App\DataFixtures\CustomerFixtures
   > loading App\DataFixtures\RecordFixtures
   > loading App\DataFixtures\YetAnotherFixtures
```
All entities will be generated with relations.

## Usage
### Swagger UI
Swagger UI should be installed as deps package. Swagger schema is generated in OpenApi 3 format.

By default it's url is: [http://localhost:8181/docs/]()

There is complete description for all endpoints available.

Swagger configuration schema is also available directly:
* in JSON: [http://localhost:8181/docs/file/_swagger/swagger.json]()
* in YAML: [http://localhost:8181/docs/file/_swagger/swagger.yml]()

### Swagger files update
Swagger files may be updated manualy after valuable changes in API:

Json:
```
docker-compose exec api ./vendor/bin/openapi --pattern "/\.php$/" --format yaml -o ./docs/_swagger/swagger.yml ./src/Controller/ ./src/DTO/
```
Yaml:
```
docker-compose exec api ./vendor/bin/openapi --pattern "/\.php$/" --format json -o ./docs/_swagger/swagger.json ./src/Controller/ ./src/DTO/
```

## Tests
Tests are splitted into 3 types.

**Note:** First time running test will trigger composer extra deps installation. 

### Preparation setup
**Note:** For the first time it's required to make new test database, usually its expected to be `<database_name>_test` name format.

For example:
* to create DB
    ```
    docker-compose exec --env DATABASE_URL="mysql://root:12345@db:3306/recordapi_test" api php bin/console doctrine:database:create --if-not-exists
    ```
**Note:** I'm providing db credentials explicitly to make sire it's `root` user with enough permissions to create new database

* to create tables
    ```
    docker-compose exec --env DATABASE_URL="mysql://root:12345@db:3306/recordapi_test" api php bin/console doctrine:migrations:migrate --env=test
    ```

### Unit-Tests
```
docker-compose exec api bin/phpunit ./tests/unit
```
Sample output:
```
PHPUnit 7.5.20 by Sebastian Bergmann and contributors.

Testing ./tests/unit
..........                                                        10 / 10 (100%)

Time: 2.34 seconds, Memory: 6.00 MB

OK (10 tests, 17 assertions)
```
### Integration-Tests
```
docker-compose exec api bin/phpunit ./tests/integration
```
Sample output:
```
PHPUnit 7.5.20 by Sebastian Bergmann and contributors.

Testing ./tests/integration
....................                                              20 / 20 (100%)

Time: 1.43 minutes, Memory: 42.50 MB

OK (20 tests, 112 assertions)
```

### Behavioral/Endpoint-Tests
```
docker-compose exec api bin/phpunit ./tests/api
```
Sample output:
```
PHPUnit 7.5.20 by Sebastian Bergmann and contributors.

Testing tests/api
.........................                                         25 / 25 (100%)

Time: 1.12 minutes, Memory: 40.00 MB

OK (25 tests, 35 assertions)
```
