<?php

namespace Tests\Api\Controller;

use App\Entity\Record;
use App\Repository\RecordRepository;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerBuilder;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class RecordControllerTest
 *
 * @package Tests\Api\Controller
 */
class RecordControllerTest extends WebTestCase
{
    /**
     * @var KernelBrowser
     */
    private KernelBrowser $client;
    /**
     * @var RecordRepository
     */
    private $repo;
    /**
     * @var Serializer
     */
    private Serializer $serializer;

    protected function setUp()
    {
        parent::setUp();
        $this->client = static::createClient();
        $this->client->setServerParameter('CONTENT_TYPE', 'application/json');
        $this->repo       = static::bootKernel()->getContainer()
                                  ->get('doctrine')
                                  ->getManager()
                                  ->getRepository(Record::class);
        $this->serializer = SerializerBuilder::create()->build();

        static::bootKernel()->getContainer()
              ->get('doctrine')
              ->getManager()
              ->createQueryBuilder()
              ->delete(Record::class, 'r')
              ->getQuery()
              ->execute();
    }

    public function testGetActionOk()
    {
        $duration = 100;
        $title    = 'Record #1';
        $obj      = $this->repo->create([
            'duration' => $duration,
            'title'    => $title,
        ]);

        $this->client->request('GET', '/record/' . $obj->getId());

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertEquals(
            sprintf(
                '{"code":1000200,"message":"record found","data":{"id":%d,"title":"%s","duration":%d,"albums":[],"artists":[]}}',
                $obj->getId(),
                $title,
                $duration
            ),
            $this->client->getResponse()->getContent()
        );
    }

    /**
     * @dataProvider getActionFailDataProvider
     *
     * @param $id
     * @param $expectedStatus
     */
    public function testGetActionFail($id, $expectedStatus)
    {
        $this->client->request('GET', '/record/' . $id);

        $this->assertEquals($expectedStatus, $this->client->getResponse()->getStatusCode());
    }

    public function getActionFailDataProvider()
    {
        return [
            [0, 400],
            ['', 404],
            ['not-exists', 400],
        ];
    }

    public function testPostActionOk()
    {
        $duration = 100;
        $title    = 'Record #1';

        $this->client->setServerParameter('CONTENT_TYPE', 'application/json');
        $this->client->request('POST', '/record', [], [], ['CONTENT_TYPE' => 'application/json'], json_encode([
            'duration' => $duration,
            'title'    => $title,
        ]));
        $obj = $this->repo->findOneBy(['title' => $title]);

        $this->assertNotEmpty($obj);
        $this->assertEquals(201, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @dataProvider postActionFailDataProvider
     *
     * @param $content
     * @param $expectedStatus
     */
    public function testPostActionFail($content, $expectedStatus)
    {
        $this->client->request('POST', '/record', [], [], [], $content);

        $this->assertEquals($expectedStatus, $this->client->getResponse()->getStatusCode());
    }

    public function postActionFailDataProvider()
    {
        return [
            [0, 400],
            ['', 400],
            ['not-exists', 400],
        ];
    }

    public function testPatchActionOk()
    {
        $duration       = 100;
        $title          = 'Record #1 for patching';
        $obj            = $this->repo->create([
            'duration' => $duration,
            'title'    => $title,
        ]);
        $updateDuration = 200;
        $updateTitle    = 'Record #1 patched';

        $this->client->setServerParameter('CONTENT_TYPE', 'application/json');
        $this->client->request('PATCH', '/record/' . $obj->getId(), [], [], ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'duration' => $updateDuration,
                'title'    => $updateTitle,
            ]));

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @dataProvider postActionFailDataProvider
     *
     * @param $content
     * @param $expectedStatus
     */
    public function testPatchActionFail($content, $expectedStatus)
    {
        $duration = 100;
        $title    = 'Record #1 for patching';
        $obj      = $this->repo->create([
            'duration' => $duration,
            'title'    => $title,
        ]);

        $this->client->request('PATCH', '/record/' . $obj->getId(), [], [], [], $content);

        $this->assertEquals($expectedStatus, $this->client->getResponse()->getStatusCode());
        $this->assertEquals('{"code":1000400,"message":"bad request"}', $this->client->getResponse()->getContent());
    }

    public function testPutActionOk()
    {
        $duration       = 100;
        $title          = 'Record #1 for put update';
        $obj            = $this->repo->create([
            'duration' => $duration,
            'title'    => $title,
        ]);
        $updateDuration = 200;
        $updateTitle    = 'Record #1 updated';

        $this->client->setServerParameter('CONTENT_TYPE', 'application/json');
        $this->client->request('PUT', '/record/' . $obj->getId(), [], [], ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'duration' => $updateDuration,
                'title'    => $updateTitle,
            ]));

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @dataProvider postActionFailDataProvider
     *
     * @param $content
     * @param $expectedStatus
     */
    public function testPutActionFail($content, $expectedStatus)
    {
        $duration = 100;
        $title    = 'Record #1 for put update';
        $obj      = $this->repo->create([
            'duration' => $duration,
            'title'    => $title,
        ]);

        $this->client->request('PUT', '/record/' . $obj->getId(), [], [], [], $content);

        $this->assertEquals($expectedStatus, $this->client->getResponse()->getStatusCode());
        $this->assertEquals('{"code":1000400,"message":"bad request"}', $this->client->getResponse()->getContent());
    }

    public function testDeleteActionOk()
    {
        $duration = 200;
        $title    = 'Record #1';

        $obj = $this->repo->create([
            'title'    => $title,
            'duration' => $duration,
        ]);
        $id  = $obj->getId();

        $this->client->request('DELETE', '/record/' . $id);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertEquals(
            sprintf(
                '{"code":1000200,"message":"record found","data":{"id":%d,"title":"%s","duration":%d,"albums":[],"artists":[]}}',
                $id,
                $title,
                $duration
            ),
            $this->client->getResponse()->getContent()
        );

        $obj = $this->repo->findOneBy(['id' => $id]);
        $this->assertNull($obj);
    }

    /**
     * @dataProvider getActionFailDataProvider
     *
     * @param $id
     * @param $expectedStatus
     */
    public function testDeleteActionFail($id, $expectedStatus)
    {
        $this->client->request('DELETE', '/record/' . $id);

        $this->assertEquals($expectedStatus, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @dataProvider searchActionOkDataProvider
     *
     * @param array $search
     * @param int   $expectedStatus
     */
    public function testSearchActionOk(array $search, int $expectedStatus)
    {
        $attributes = $search;
        if (!isset($search['title'])) {
            $attributes['title'] = 'added title';
        }
        if (!isset($search['duration'])) {
            $attributes['duration'] = 120;
        }

        $this->repo->create($attributes);

        $this->client->request('POST', '/record/search', [], [], [], json_encode($search));

        $this->assertEquals($expectedStatus, $this->client->getResponse()->getStatusCode());
    }

    public function searchActionOkDataProvider()
    {
        return [
            [['title' => 'record title'], 200],
            [['abc' => 'def'], 404],
            [['abc' => 'def', 'title' => 'record title'], 200],
            [['duration' => 60, 'title' => 'record title'], 200],
            [['ijk'=>'lmn', 'duration' => 90, 'title' => 'record title'], 200],
        ];
    }
}
