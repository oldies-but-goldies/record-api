<?php

namespace Tests\Integration\Repository;

use App\Entity\Record;
use App\Repository\RecordRepository;
use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\ORMInvalidArgumentException;
use Exception;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Class RecordRepositoryTest
 *
 * @package Tests\Integration\Repository
 */
class RecordRepositoryTest extends KernelTestCase
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var RecordRepository $repo
     */
    private $repo;

    protected function setUp(): void
    {
        $kernel     = self::bootKernel();
        $this->em   = $kernel->getContainer()
                             ->get('doctrine')
                             ->getManager();
        $this->repo = $this->em->getRepository(Record::class);

        $this->em->createQueryBuilder()
                 ->delete(Record::class, 'r')
                 ->getQuery()
                 ->execute();
    }

    /**
     * @dataProvider createRecordDataProvider
     *
     * @param $duration
     * @param $title
     *
     * @throws ORMException
     */
    public function testCreateMethodOk($duration, $title)
    {
        $obj = $this->repo->create([
            'duration' => $duration,
            'title'    => $title,
        ]);

        $this->assertNotEmpty($obj);
        $this->assertNotEmpty($obj->getId());
        $this->assertInstanceOf(Record::class, $obj);
        $this->assertEquals($duration, $obj->getDuration());
        $this->assertEquals($title, $obj->getTitle());
    }

    /**
     * @return array[]
     * @throws Exception
     */
    public function createRecordDataProvider()
    {
        return [
            'rand-title-1' => [
                0,
                md5(random_bytes(32)),
            ],
            'rand-title-2' => [
                10,
                md5(random_bytes(32)),
            ],
            'rand-title-3' => [
                100,
                md5(random_bytes(32)),
            ],
        ];
    }

    /**
     * @dataProvider createRecordFailDataProvider
     *
     * @param $attributes
     *
     * @throws ORMException
     */
    public function testCreateMethodFail($attributes)
    {
        $this->expectException(NotNullConstraintViolationException::class);

        $this->repo->create($attributes);
    }

    /**
     * @return array[]
     * @throws Exception
     */
    public function createRecordFailDataProvider()
    {
        return [
            'no-input'                 => [null],
            'partly-missing: duration' => [['title' => 'a title']],
            'partly-missing: title'    => [['duration' => 123]],
        ];
    }


    /**
     * @dataProvider updateRecordDataProvider
     *
     * @param $duration
     * @param $title
     * @param $updateDuration
     * @param $updateTitle
     *
     * @throws ORMException
     */
    public function testUpdateMethodOk($duration, $title, $updateDuration, $updateTitle)
    {
        //create new
        $obj = $this->repo->create([
            'duration' => $duration,
            'title'    => $title,
        ]);
        $id  = $obj->getId();

        $this->assertNotEmpty($obj);
        $this->assertNotEmpty($id);
        $this->assertInstanceOf(Record::class, $obj);
        $this->assertEquals($duration, $obj->getDuration());
        $this->assertEquals($title, $obj->getTitle());

        //update
        $obj = $this->repo->update(
            $obj,
            [
                'duration' => $updateDuration,
                'title'    => $updateTitle,
            ]
        );

        $this->assertEquals($id, $obj->getId());
        $this->assertEquals($updateDuration, $obj->getDuration());
        $this->assertEquals($updateTitle, $obj->getTitle());
    }

    /**
     * @return array[]
     * @throws Exception
     */
    public function updateRecordDataProvider()
    {
        return [
            // duration, title, update duration, update title
            'rand-title-1' => [
                0,
                md5(random_bytes(32)),
                20,
                md5(random_bytes(32)),
            ],
            'rand-title-2' => [
                10,
                md5(random_bytes(32)),
                200,
                md5(random_bytes(32)),
            ],
            'rand-title-3' => [
                100,
                md5(random_bytes(32)),
                2000,
                md5(random_bytes(32)),
            ],
        ];
    }

    /**
     * @dataProvider createRecordFailDataProvider
     *
     * @param $attributes
     *
     * @throws ORMException
     */
    public function testUpdateMethodPartlyOk($attributes)
    {
        $obj = $this->repo->create([
            'duration' => 123,
            'title'    => 'created record',
        ]);
        $id  = $obj->getId();

        $this->assertNotEmpty($obj);
        $this->assertNotEmpty($id);
        $this->assertInstanceOf(Record::class, $obj);

        $obj = $this->repo->update($obj, $attributes);

        $this->assertEquals($id, $obj->getId());
        if (isset($attributes['duration'])) {
            $this->assertEquals($attributes['duration'], $obj->getDuration());
        }
        if (isset($attributes['title'])) {
            $this->assertEquals($attributes['title'], $obj->getTitle());
        }
    }

    /**
     * @dataProvider createRecordDataProvider
     *
     * @param $duration
     * @param $title
     *
     * @throws ORMException
     */
    public function testDeleteMethodOk($duration, $title)
    {
        $obj = $this->repo->create([
            'duration' => $duration,
            'title'    => $title,
        ]);
        $id  = $obj->getId();

        $this->assertNotEmpty($obj);
        $this->assertNotEmpty($obj->getId());
        $this->assertInstanceOf(Record::class, $obj);

        $this->repo->delete($obj);

        $this->assertNull($this->repo->find($id));
    }

    /**
     * @throws ORMException
     */
    public function testDeleteMethodFail()
    {
        $this->expectException(ORMInvalidArgumentException::class);

        $this->repo->delete(new Record(['id' => -1]));
    }


    /**
     * @dataProvider searchRecordDataProvider
     *
     * @param $duration
     * @param $title
     *
     * @throws ORMException
     * @throws Exception
     */
    public function testSearchMethodOk($duration, $title)
    {
        // create all
        $fixturesData = $this->searchRecordDataProvider();
        foreach ($fixturesData as $data) {
            $this->repo->create([
                'duration' => $data[0],
                'title'    => $data[1],
            ]);
        }

        //search existing one
        $found = $this->repo->search(['duration' => $duration]);
        $id    = $found[0]->getId();

        $this->assertNotEmpty($found);
        $this->assertCount(1, $found);
        $this->assertNotEmpty($found[0]);
        $this->assertNotEmpty($found[0]->getId());
        $this->assertInstanceOf(Record::class, $found[0]);
        $this->assertEquals($duration, $found[0]->getDuration());

        $found = $this->repo->search(['title' => $title]);

        $this->assertNotEmpty($found);
        $this->assertCount(1, $found);
        $this->assertNotEmpty($found[0]);
        $this->assertNotEmpty($found[0]->getId());
        $this->assertInstanceOf(Record::class, $found[0]);
        $this->assertEquals($id, $found[0]->getId());
        $this->assertEquals($title, $found[0]->getTitle());
    }

    public function testSearchMethodSeveralOk()
    {
        $duration = 10;
        $title    = 'record number two';

        // create all
        $fixturesData = $this->searchRecordDataProvider();
        foreach ($fixturesData as $data) {
            $this->repo->create([
                'duration' => $data[0],
                'title'    => $data[1],
            ]);
        }
        // plus duplicates
        //      by duration:
        $this->repo->create([
            'duration' => $duration,
            'title'    => 'record with same duration',
        ]);
        //      by title:
        $this->repo->create([
            'duration' => 20,
            'title'    => $title,
        ]);

        //search existing by duration
        $found = $this->repo->search(['duration' => $duration]);
        $this->assertNotEmpty($found);
        $this->assertCount(2, $found);

        //search existing by title
        $found = $this->repo->search(['title' => $title]);
        $this->assertNotEmpty($found);
        $this->assertCount(2, $found);
    }

    /**
     * @return array[]
     * @throws Exception
     */
    public function searchRecordDataProvider()
    {
        return [
            'record-1' => [
                0,
                'record #1',
            ],
            'record-2' => [
                10,
                'record number two',
            ],
            'record-3' => [
                100,
                'record ###3',
            ],
        ];
    }
}
