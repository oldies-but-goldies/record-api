<?php

namespace Tests\Unit\Entity;

use App\Abstracts\EntityAbstract;
use App\Entity\Record;
use PHPUnit\Framework\TestCase;

/**
 * Class RecordTest
 *
 * @package Tests\Unit\Entity
 */
class RecordTest extends TestCase
{
    public function testObject()
    {
        $obj = new Record();
        $this->assertInstanceOf(EntityAbstract::class, $obj);
        $this->assertTrue(method_exists($obj, 'setterByName'));
    }

    /**
     * @dataProvider attributesDataProvider
     *
     * @param $attributes
     */
    public function testObjectCreate($attributes)
    {
        $obj = new Record($attributes);
        isset($attributes['title'])
            ? $this->assertEquals($attributes['title'], $obj->getTitle())
            : $this->assertEmpty($obj->getTitle());
        isset($attributes['duration'])
            ? $this->assertEquals($attributes['duration'], $obj->getDuration())
            : $this->assertEmpty($obj->getDuration());
    }

    public function attributesDataProvider()
    {
        return [
            [['' => 'abc']],
            [['a' => 'abc', 'b' => 123]],
            [['title' => 'title 1', 'c' => 'def']],
            [['d' => 'gij', 'title' => 'title 2']],
            [['duration' => 100, 'c' => 'def']],
            [['d' => 'gij', 'duration' => 200]],
            [['title' => 'gij', 'duration' => 20]],
        ];
    }

    /**
     * @dataProvider nameDataProvider
     *
     * @param $name
     * @param $expectedSetterName
     */
    public function testSetterByNameMethod($name, $expectedSetterName)
    {
        $obj = new Record();
        $this->assertEquals($expectedSetterName, $obj::setterByName($name));
    }

    public function nameDataProvider()
    {
        return [
            ['', 'set'],
            ['abc', 'setAbc'],
            ['abc_def', 'setAbcDef'],
        ];
    }
}
