<?php

namespace App\Abstracts;

use Swagger\Server\Model\RecordApiResponse;

/**
 * Class RecordRestControllerAbstract
 *
 * @package App\Abstracts
 */
abstract class RecordRestControllerAbstract extends RestControllerAbstract
{
    protected const API_RESPONSE_CLASS = RecordApiResponse::class;
}
