<?php

namespace App\Abstracts;

/**
 * Class EntityAbstract
 *
 * @package App\Abstracts
 */
abstract class EntityAbstract
{
    /**
     * Constructor
     *
     * @param array|null $attributes
     */
    public function __construct(array $attributes = null)
    {
        if (!empty($attributes)) {
            foreach ($attributes as $k => $v) {
                if (property_exists(static::class, $k)) {
                    $this->{static::setterByName($k)}($v);
                }
            }
        }
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Helper method to convert property name into setter name
     *
     * @param string $name
     *
     * @return string
     */
    public static function setterByName(string $name): string
    {
        return 'set' . str_replace('_', '', ucwords($name, '_'));
    }
}
