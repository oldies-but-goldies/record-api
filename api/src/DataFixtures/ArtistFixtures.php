<?php

namespace App\DataFixtures;

use App\Entity\Artist;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Provider\Person;

/**
 * Class ArtistFixtures
 *
 * @package App\DataFixtures
 */
class ArtistFixtures extends Fixture
{
    public const TOTAL = 1000;

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('en_GB');
        for ($i = 0; $i < self::TOTAL; $i++) {
            $gender = [Person::GENDER_MALE, Person::GENDER_FEMALE,][Person::numberBetween(0, 1)];
            $manager->getRepository(Artist::class)->create([
                'name' => $faker->name($gender)
            ]);
        }
        $manager->flush();
    }
}
