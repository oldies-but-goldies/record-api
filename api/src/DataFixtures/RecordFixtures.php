<?php

namespace App\DataFixtures;

use App\Entity\Record;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

/**
 * Class RecordFixtures
 *
 * @package App\DataFixtures
 */
class RecordFixtures extends Fixture
{
    public const TOTAL = 5000;

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('en_GB');
        for ($i = 0; $i < self::TOTAL; $i++) {
            $manager->getRepository(Record::class)->create([
                'title'    => $faker->text(50),
                'duration' => $faker->numberBetween(20, 300),
            ]);
        }
        $manager->flush();
    }
}
