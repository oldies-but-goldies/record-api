<?php

namespace App\DataFixtures;

use App\Entity\Album;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

/**
 * Class AlbumFixtures
 *
 * @package App\DataFixtures
 */
class AlbumFixtures extends Fixture
{
    public const TOTAL = 250;

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('en_GB');

        for ($i = 0; $i < self::TOTAL; $i++) {
            $obj = $manager->getRepository(Album::class)->create([
                'title'       => $faker->text(50),
                'description' => $faker->realText(150),
            ]);
        }
        $manager->flush();
    }
}
