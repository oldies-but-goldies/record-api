<?php

namespace App\DataFixtures;

use App\Entity\Album;
use App\Entity\Artist;
use App\Entity\Record;
use App\Repository\AlbumRepository;
use App\Repository\ArtistRepository;
use App\Repository\RecordRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

/**
 * Class YetAnotherFixtures
 *
 * @package App\DataFixtures
 */
class YetAnotherFixtures extends Fixture
{
    public const MIN_RECORDS_IN_ALBUM = 5;
    public const MAX_RECORDS_IN_ALBUM = 20;
    public const MIN_ARTISTS_IN_ALBUM = 1;
    public const MAX_ARTISTS_IN_ALBUM = 3;

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('en_GB');

        /** @var RecordRepository $recordRepo */
        $recordRepo = $manager->getRepository(Record::class);
        /** @var []Record $allRecords */
        $allRecords = $recordRepo->findAll();

        /** @var AlbumRepository $albumRepo */
        $albumRepo = $manager->getRepository(Album::class);
        /** @var []Album $allAlbums */
        $allAlbums = $albumRepo->findAll();

        /** @var ArtistRepository $artistRepo */
        $artistRepo = $manager->getRepository(Artist::class);
        /** @var []Artist $allArtists */
        $allArtists = $artistRepo->findAll();

        $noRecs = false;
        foreach ($allAlbums as $album) {

            $totalArtists = $faker->numberBetween(self::MIN_ARTISTS_IN_ALBUM, self::MAX_ARTISTS_IN_ALBUM);
            $artists      = [];
            $i            = 0;
            $item         = current($allArtists);
            while ($i < $totalArtists) {
                if ($item === false) {
                    reset($allArtists);
                    $item = current($allArtists);
                }
                $artistRepo->linkAlbum($item, $album);
                $artists[] = $item;
                $item      = next($allArtists);
                $i++;
            }

            $totalRecords = $faker->numberBetween(self::MIN_RECORDS_IN_ALBUM, self::MAX_RECORDS_IN_ALBUM);
            $i            = 0;
            $item         = current($allRecords);
            while ($i < $totalRecords) {
                if ($item === false) {
                    $noRecs = true;
                    break;
                }
                $albumRepo->linkRecord($album, $item);

                $artist = $artists[$faker->numberBetween(0, count($artists) - 1)];
                $artistRepo->linkRecord($artist, $item);
                $item = next($allRecords);
                $i++;
            }

            if ($noRecs) {
                break;
            }
        }

        //utilizing rest records
        if (!$noRecs && $item !== false) {
            while ($item !== false) {
                $totalArtists = $faker->numberBetween(self::MIN_ARTISTS_IN_ALBUM, self::MAX_ARTISTS_IN_ALBUM);
                $i            = 0;
                $artist       = current($allArtists);
                while ($i < $totalArtists) {
                    if ($artist === false) {
                        reset($allArtists);
                        $artist = current($allArtists);
                    }
                    $artistRepo->linkRecord($artist, $item);
                    $artist = next($allArtists);
                    $i++;
                }
                $item = next($allRecords);
            }
        }

        $manager->flush();
    }
}
