<?php

namespace App\DataFixtures;

use App\Entity\Customer;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

/**
 * Class CustomerFixtures
 *
 * @package App\DataFixtures
 */
class CustomerFixtures extends Fixture
{
    public const TOTAL = 100;

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('en_GB');
        for ($i = 0; $i < self::TOTAL; $i++) {
            $email = null;
            switch ($faker->numberBetween(0, 3)) {
                case 0;
                    $email = $faker->email;
                    break;
                case 1;
                    $email = $faker->freeEmail;
                    break;
                case 2;
                    $email = $faker->safeEmail;
                    break;
                case 3;
                    $email = $faker->companyEmail;
                    break;
            }
            $manager->getRepository(Customer::class)->create([
                'email' => $email
            ]);
        }
        $manager->flush();
    }
}
