<?php

namespace App\Repository;

use App\Entity\Album;
use App\Entity\Artist;
use App\Entity\ArtistAlbums;
use App\Entity\ArtistRecords;
use App\Entity\Record;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class ArtistRepository
 *
 * @package App\Repository
 */
class ArtistRepository extends ServiceEntityRepository
{
    /**
     * ArtistRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Artist::class);
    }

    /**
     * @param array|null $attributes
     *
     * @return Artist
     * @throws ORMException
     */
    public function create(array $attributes = null): Artist
    {
        $object = new Artist($attributes);
        $this->getEntityManager()->persist($object);

        return $object;
    }

    /**
     * @param Artist $artist
     * @param Album  $album
     *
     * @throws ORMException
     */
    public function linkAlbum(Artist $artist, Album $album)
    {
        $object = new ArtistAlbums($artist, $album);
        $this->getEntityManager()->persist($object);
    }

    /**
     * @param Artist $artist
     * @param Record $record
     *
     * @throws ORMException
     */
    public function linkRecord(Artist $artist, Record $record)
    {
        $object = new ArtistRecords($artist, $record);
        $this->getEntityManager()->persist($object);
    }
}
