<?php

namespace App\Repository;

use App\Entity\Record;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class RecordRepository
 *
 * @package App\Repository
 */
class RecordRepository extends ServiceEntityRepository
{
    /**
     * RecordRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Record::class);
    }

    /**
     * @param array|null $attributes
     *
     * @return Record
     * @throws ORMException
     */
    public function create(array $attributes = null): Record
    {
        $object = new Record($attributes);
        $this->getEntityManager()->persist($object);
        $this->getEntityManager()->flush();

        return $object;
    }

    /**
     * @param Record     $object
     * @param array|null $attributes
     *
     * @return Record
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function update(Record $object, array $attributes = null): Record
    {
        if (empty($attributes)) {
            return $object;
        }

        foreach ($attributes as $k => $v) {
            $method = Record::setterByName($k);
            if (method_exists($object, $method)) {
                $object->{$method}($v);
            }
        }
        $this->getEntityManager()->persist($object);
        $this->getEntityManager()->flush();

        return $object;
    }

    /**
     * @param Record $object
     *
     * @throws ORMException
     */
    public function delete(Record $object)
    {
        $this->getEntityManager()->remove($object);
        $this->getEntityManager()->flush();
    }

    /**
     * @param array $criteria
     *
     * @return array|Record[]
     * @todo To write alternative method with Query Builder
     */
    public function search(array $criteria): array
    {
        return $this->findBy($criteria);
    }
}
