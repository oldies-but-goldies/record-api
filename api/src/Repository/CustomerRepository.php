<?php

namespace App\Repository;

use App\Entity\Customer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class CustomerRepository
 *
 * @package App\Repository
 */
class CustomerRepository extends ServiceEntityRepository
{
    /**
     * CustomerRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Customer::class);
    }

    /**
     * @param array|null $attributes
     *
     * @return Customer
     * @throws ORMException
     */
    public function create(array $attributes = null): Customer
    {
        $object = new Customer($attributes);
        $this->getEntityManager()->persist($object);
        $this->getEntityManager()->persist($object);

        return $object;
    }
}
