<?php

namespace App\Repository;

use App\Abstracts\EntityAbstract;
use App\Entity\Album;
use App\Entity\Customer;
use App\Entity\Record;
use App\Entity\ShoppingCart;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class ShoppingCartRepository
 *
 * @package App\Repository
 */
class ShoppingCartRepository extends ServiceEntityRepository
{
    private const MAP_TYPES = [
        Album::class  => 'album',
        Record::class => 'record',
    ];

    /**
     * ShoppingCartRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ShoppingCart::class);
    }

    /**
     * @param array|null $attributes
     *
     * @return ShoppingCart
     * @throws ORMException
     */
    public function create(array $attributes = null): ShoppingCart
    {
        $object = new ShoppingCart($attributes);
        $this->getEntityManager()->persist($object);

        return $object;
    }

    /**
     * @param EntityAbstract $item
     * @param Customer       $customer
     *
     * @return ShoppingCart|null
     */
    public function addItem(EntityAbstract $item, Customer $customer): ?ShoppingCart
    {
        if (empty(self::MAP_TYPES[get_class($item)])) {
            return null;
        }

        return $this->create([
            'id_customer' => $customer->getId(),
            'type'        => self::MAP_TYPES[get_class($item)],
            'reference'   => $item->getId(),
        ]);
    }
}
