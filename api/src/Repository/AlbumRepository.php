<?php

namespace App\Repository;

use App\Entity\Album;
use App\Entity\AlbumRecords;
use App\Entity\Record;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class AlbumRepository
 *
 * @package App\Repository
 */
class AlbumRepository extends ServiceEntityRepository
{
    /**
     * AlbumRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Album::class);
    }

    /**
     * @param array|null $attributes
     *
     * @return Album
     * @throws ORMException
     */
    public function create(array $attributes = null): Album
    {
        $object = new Album($attributes);
        $this->getEntityManager()->persist($object);

        return $object;
    }

    /**
     * @param Album  $album
     * @param Record $record
     *
     * @throws ORMException
     */
    public function linkRecord(Album $album, Record $record)
    {
        $object = new AlbumRecords($album, $record);
        $this->getEntityManager()->persist($object);
    }
}
