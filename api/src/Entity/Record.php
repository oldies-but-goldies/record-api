<?php

namespace App\Entity;

use App\Abstracts\EntityAbstract;
use App\Repository\RecordRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Record
 *
 * @ORM\Table(name="record")
 * @ORM\Entity(repositoryClass=RecordRepository::class)
 */
class Record extends EntityAbstract
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=256, nullable=false)
     */
    private $title;

    /**
     * @var int
     *
     * @ORM\Column(name="duration", type="integer", nullable=false)
     */
    private $duration;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Album", mappedBy="idRecord")
     */
    private $idAlbum;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Artist", mappedBy="idRecord")
     */
    private $idArtist;

    /**
     * @inheritDoc
     */
    public function __construct(array $attributes = null)
    {
        parent::__construct($attributes);

        $this->idAlbum  = new ArrayCollection();
        $this->idArtist = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getDuration(): ?int
    {
        return $this->duration;
    }

    /**
     * @param int $duration
     *
     * @return self
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getAllAlbums(): Collection
    {
        return $this->idAlbum;
    }

    /**
     * @return Collection
     */
    public function getAllArtists(): Collection
    {
        return $this->idArtist;
    }
}
