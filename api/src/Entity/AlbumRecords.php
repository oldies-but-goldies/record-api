<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ArtistRecords
 *
 * @ORM\Table(name="album_records")
 * @ORM\Entity
 */
class AlbumRecords
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_album", type="integer", nullable=false)
     * @ORM\Id
     */
    private $id_album;

    /**
     * @var int
     *
     * @ORM\Column(name="id_record", type="integer", nullable=false)
     * @ORM\Id
     */
    private $id_record;

    /**
     * @param Album  $album
     * @param Record $record
     */
    public function __construct(Album $album, Record $record)
    {
        $this->id_album  = $album->getId();
        $this->id_record = $record->getId();
    }
}
