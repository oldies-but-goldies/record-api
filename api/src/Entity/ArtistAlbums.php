<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ArtistAlbums
 *
 * @ORM\Table(name="artist_albums")
 * @ORM\Entity
 */
class ArtistAlbums
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_artist", type="integer", nullable=false)
     * @ORM\Id
     */
    private $id_artist;

    /**
     * @var int
     *
     * @ORM\Column(name="id_album", type="integer", nullable=false)
     * @ORM\Id
     */
    private $id_album;

    /**
     * @param Artist $artist
     * @param Album  $album
     */
    public function __construct(Artist $artist, Album $album)
    {
        $this->id_artist = $artist->getId();
        $this->id_album  = $album->getId();
    }
}
