<?php

namespace App\Entity;

use App\Abstracts\EntityAbstract;
use App\Repository\ArtistRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Artist
 *
 * @ORM\Table(name="artist")
 * @ORM\Entity(repositoryClass=ArtistRepository::class)
 */
class Artist extends EntityAbstract
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=256, nullable=false)
     */
    private $name;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Album", inversedBy="idArtist")
     * @ORM\JoinTable(name="artist_albums",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_artist", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_album", referencedColumnName="id")
     *   }
     * )
     */
    private $idAlbum;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Record", inversedBy="idArtist")
     * @ORM\JoinTable(name="artist_records",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_artist", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_record", referencedColumnName="id")
     *   }
     * )
     */
    private $idRecord;

    /**
     * @inheritDoc
     */
    public function __construct(array $attributes = null)
    {
        parent::__construct($attributes);

        $this->idAlbum  = new ArrayCollection();
        $this->idRecord = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getAllAlbums(): Collection
    {
        return $this->idAlbum;
    }

    /**
     * @return Collection
     */
    public function getAllRecords(): Collection
    {
        return $this->idRecord;
    }
}
