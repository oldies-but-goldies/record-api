<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ArtistRecords
 *
 * @ORM\Table(name="artist_records")
 * @ORM\Entity
 */
class ArtistRecords
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_artist", type="integer", nullable=false)
     * @ORM\Id
     */
    private $id_artist;

    /**
     * @var int
     *
     * @ORM\Column(name="id_record", type="integer", nullable=false)
     * @ORM\Id
     */
    private $id_record;

    /**
     * @param Artist $artist
     * @param Record $record
     */
    public function __construct(Artist $artist, Record $record)
    {
        $this->id_artist = $artist->getId();
        $this->id_record = $record->getId();
    }
}
