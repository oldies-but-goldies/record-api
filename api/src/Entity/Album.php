<?php

namespace App\Entity;

use App\Abstracts\EntityAbstract;
use App\Repository\AlbumRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Album
 *
 * @ORM\Table(name="album")
 * @ORM\Entity(repositoryClass=AlbumRepository::class)
 */
class Album extends EntityAbstract
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=256, nullable=false)
     */
    private $title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Record", inversedBy="idAlbum")
     * @ORM\JoinTable(name="album_records",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_album", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_record", referencedColumnName="id")
     *   }
     * )
     */
    private $idRecord;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Artist", mappedBy="idAlbum")
     */
    private $idArtist;

    /**
     * @inheritDoc
     */
    public function __construct(array $attributes = null)
    {
        parent::__construct($attributes);

        $this->idRecord = new ArrayCollection();
        $this->idArtist = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     *
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getAllRecords(): Collection
    {
        return $this->idRecord;
    }

    /**
     * @return Collection
     */
    public function getAllArtists(): Collection
    {
        return $this->idArtist;
    }

}
