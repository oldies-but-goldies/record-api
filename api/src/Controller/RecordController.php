<?php

namespace App\Controller;

use App\Abstracts\RecordRestControllerAbstract;
use App\Entity\Album;
use App\Entity\Artist;
use App\Entity\Record;
use Swagger\Server\Model\DTOAlbum;
use Swagger\Server\Model\DTOArtist;
use Swagger\Server\Model\DTOSearchRecord;
use Swagger\Server\Model\RecordApiResponse;
use Swagger\Server\Model\DTORecord;
use Swagger\Server\Model\SearchRecordApiResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use OpenApi\Annotations as OA;
use Throwable;

/**
 * Class RecordController
 *
 * @package App\Controller
 */
class RecordController extends RecordRestControllerAbstract
{
    const CODE_OK          = 1000200;
    const CODE_CREATED     = 1000201;
    const CODE_NOT_FOUND   = 1000404;
    const CODE_BAD_REQUEST = 1000400;
    const MSG_FOUND        = 'record found';
    const MSG_FOUND_MANY   = 'records found';
    const MSG_CREATED      = 'record created';
    const MSG_NOT_FOUND    = 'record not found';
    const MSG_BAD_REQUEST  = 'bad request';

    /**
     * @OA\Get(
     *   tags={"Record"},
     *   path="/record/{id}",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="number"),
     *     description="ID value"
     *   ),
     *   @OA\Response(
     *       response="default",
     *       description="successful operation",
     *       @OA\JsonContent(ref="#/components/schemas/RecordApiResponse")
     *   )
     * )
     *
     * @param string $id
     *
     * @return JsonResponse
     */
    public function getAction(string $id)
    {
        if (empty($id) || !is_numeric($id)) {
            return $this->badRequestJson();
        }

        /** @var Record|false $obj */
        $obj = $this->getDoctrine()->getRepository(Record::class)->find($id);
        if (empty($obj)) {
            return $this->notFoundJson();
        }

        return $this->okJson(new DTORecord([
            'id'       => $id,
            'title'    => $obj->getTitle(),
            'duration' => $obj->getDuration(),
        ]));
    }

    /**
     * @OA\Post(
     *   tags={"Record"},
     *   path="/record",
     *   @OA\RequestBody(
     *     required=true,
     *     description="Record data to create",
     *     @OA\JsonContent(ref="#/components/schemas/DTORecord")
     *   ),
     *   @OA\Response(
     *       response=201,
     *       description="successful operation",
     *       @OA\JsonContent(ref="#/components/schemas/RecordApiResponse")
     *   )
     * )
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function postAction(Request $request)
    {
        $jsonContent = $request->getContent();

        if (empty($jsonContent)) {
            return $this->badRequestJson();
        }
        try {
            /** @var DTORecord $json */
            $json = $this->serializer->deserialize($jsonContent, DTORecord::class, 'json');
        } catch (Throwable $e) {
            return $this->badRequestJson();
        }

        /** @var Record|false $obj */
        $obj = $this->getDoctrine()->getRepository(Record::class)->create([
            'title'    => $json->getTitle(),
            'duration' => $json->getDuration(),
        ]);
        if (empty($obj)) {
            return $this->notFoundJson();
        }

        return $this->createdJson(new DTORecord([
            'id'       => $obj->getId(),
            'title'    => $obj->getTitle(),
            'duration' => $obj->getDuration(),
        ]));
    }

    /**
     * @OA\Patch(
     *   tags={"Record"},
     *   path="/record/{id}",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="number"),
     *     description="ID value"
     *   ),
     *   @OA\RequestBody(
     *     required=true,
     *     description="Record data for partial update",
     *     @OA\JsonContent(ref="#/components/schemas/DTORecord")
     *   ),
     *   @OA\Response(
     *       response=200,
     *       description="successful operation",
     *       @OA\JsonContent(ref="#/components/schemas/RecordApiResponse")
     *   )
     * )
     *
     * @param Request $request
     * @param string  $id
     *
     * @return JsonResponse
     */
    public function patchAction(Request $request, string $id)
    {
        $jsonContent = $request->getContent();

        if (empty($jsonContent)) {
            return $this->badRequestJson();
        }
        try {
            /** @var DTORecord $json */
            $json = $this->serializer->deserialize($jsonContent, DTORecord::class, 'json');
        } catch (Throwable $e) {
            return $this->badRequestJson();
        }
        $attributes = array_filter($json->toArray(), fn($v) => !empty($v));

        /** @var Record|false $obj */
        $obj = $this->getDoctrine()->getRepository(Record::class)->find($id);
        if (empty($obj)) {
            return $this->notFoundJson();
        }

        $this->getDoctrine()->getRepository(Record::class)->update(
            $obj,
            $attributes
        );
        if (empty($obj)) {
            return $this->notFoundJson();
        }

        return $this->okJson(new DTORecord([
            'id'       => $obj->getId(),
            'title'    => $obj->getTitle(),
            'duration' => $obj->getDuration(),
        ]));
    }

    /**
     * @OA\Put(
     *   tags={"Record"},
     *   path="/record/{id}",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="number"),
     *     description="ID value"
     *   ),
     *   @OA\RequestBody(
     *     required=true,
     *     description="Record data for full update",
     *     @OA\JsonContent(ref="#/components/schemas/DTORecord")
     *   ),
     *   @OA\Response(
     *       response=200,
     *       description="successful operation",
     *       @OA\JsonContent(ref="#/components/schemas/RecordApiResponse")
     *   )
     * )
     *
     * @param Request $request
     * @param string  $id
     *
     * @return JsonResponse
     */
    public function putAction(Request $request, string $id)
    {
        $jsonContent = $request->getContent();

        if (empty($jsonContent)) {
            return $this->badRequestJson();
        }
        try {
            /** @var DTORecord $json */
            $json = $this->serializer->deserialize($jsonContent, DTORecord::class, 'json');
        } catch (Throwable $e) {
            return $this->badRequestJson();
        }
        $json->setId($id);

        /** @var Record|false $obj */
        $obj = $this->getDoctrine()->getRepository(Record::class)->find($id);
        if (empty($obj)) {
            return $this->notFoundJson();
        }

        $this->getDoctrine()->getRepository(Record::class)->update(
            $obj,
            $json->toArray()
        );
        if (empty($obj)) {
            return $this->notFoundJson();
        }

        return $this->okJson(new DTORecord([
            'id'       => $obj->getId(),
            'title'    => $obj->getTitle(),
            'duration' => $obj->getDuration(),
        ]));
    }

    /**
     * @OA\Delete(
     *   tags={"Record"},
     *   path="/record/{id}",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="number"),
     *     description="ID value"
     *   ),
     *   @OA\Response(
     *       response="default",
     *       description="successful operation",
     *       @OA\JsonContent(ref="#/components/schemas/RecordApiResponse")
     *   )
     * )
     *
     * @param string $id
     *
     * @return JsonResponse
     */
    public function deleteAction(string $id)
    {
        if (empty($id) || !is_numeric($id)) {
            return $this->badRequestJson();
        }

        /** @var Record|false $obj */
        $obj = $this->getDoctrine()->getRepository(Record::class)->find($id);
        if (empty($obj)) {
            return $this->notFoundJson();
        }

        $this->getDoctrine()->getRepository(Record::class)->delete($obj);

        return $this->okJson(new DTORecord([
            'id'       => $id,
            'title'    => $obj->getTitle(),
            'duration' => $obj->getDuration(),
        ]));
    }

    /**
     * @OA\Post(
     *   tags={"Record"},
     *   path="/record/search",
     *   @OA\RequestBody(
     *     required=true,
     *     description="Record data to search",
     *     @OA\JsonContent(ref="#/components/schemas/DTOSearchRecord")
     *   ),
     *   @OA\Response(
     *       response=200,
     *       description="successful operation",
     *       @OA\JsonContent(ref="#/components/schemas/SearchRecordApiResponse")
     *   )
     * )
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function searchAction(Request $request)
    {
        $jsonContent = $request->getContent();

        if (empty($jsonContent)) {
            return $this->badRequestJson();
        }
        try {
            /** @var DTORecord $json */
            $json = $this->serializer->deserialize($jsonContent, DTOSearchRecord::class, 'json');
        } catch (Throwable $e) {
            return $this->badRequestJson();
        }

        $criteria = [];
        if (!empty($json->getTitle())) {
            $criteria['title'] = $json->getTitle();
        }
        if (!empty($json->getDuration())) {
            $criteria['duration'] = $json->getDuration();
        }

        if (empty($criteria)) {
            return $this->notFoundJson();
        }

        /** @var Record[]|false $obj */
        $found = $this->getDoctrine()->getRepository(Record::class)->search($criteria);
        if (empty($found)) {
            return $this->notFoundJson();
        }

        return $this->searchJson($found);
    }

    /**
     * General Search data response
     * @todo To use model-level relations is not oprimal way, should be replaced by custom request via Query Builder
     *
     * @param array $data
     *
     * @return JsonResponse
     */
    protected function searchJson(array $data): JsonResponse
    {
        $dtoData = [];
        /** @var Record $obj */
        foreach ($data as $obj) {
            $albumsData = [];
            $albums     = $obj->getAllAlbums();
            /** @var Album $album */
            foreach ($albums as $album) {
                $albumsData[] = new DTOAlbum([
                    'id'          => $album->getId(),
                    'title'       => $album->getTitle(),
                    'description' => $album->getDescription(),
                ]);
            }

            $artistsData = [];
            $artists     = $obj->getAllArtists();
            /** @var Artist $artist */
            foreach ($artists as $artist) {
                $artistsData[] = new DTOArtist([
                    'id'   => $artist->getId(),
                    'name' => $artist->getName(),
                ]);
            }

            // a key for sorting, easy to extend sort with album name too
            $key =  (count($artistsData) ? $artistsData[0]->getName() : '') . '|' . $obj->getTitle();
            $dtoData[$key] = new DTORecord([
                'id'       => $obj->getId(),
                'title'    => $obj->getTitle(),
                'duration' => $obj->getDuration(),
                'albums'   => $albumsData,
                'artists'  => $artistsData,
            ]);
        }

        //implementing the case - The results must be listed in alphabetical order by artist and record title.
        ksort($dtoData);

        $json        = new SearchRecordApiResponse([
            'code'    => static::CODE_OK,
            'message' => static::MSG_FOUND_MANY,
            'data'    => $dtoData,
        ]);
        $jsonContent = $this->serializer->serialize($json, 'json');

        return new JsonResponse($jsonContent, Response::HTTP_OK, [], true);
    }
}
