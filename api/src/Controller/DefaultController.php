<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

/**
 * Class DefaultController
 *
 * @package App\Controller
 */
class DefaultController extends AbstractController
{
    /**
     * @OA\Info(
     *   title="Record Rest API",
     *   version="1.0.0",
     *   description="The vintage record shop 'Oldies but Goldies Berlin' needs a new website. They want to have all their record collection available for their customers to be consulted online.\n\nCustomers will be able to see the list of records and search by several fields. The record list will be maintained by the shop administrators.",
     *   @OA\Contact(
     *     name="Alex",
     *     email="alex@webz.asia"
     *   ),
     *   @OA\License(
     *     name="Apache 2.0",
     *     url="http://www.apache.org/licenses/LICENSE-2.0.html"
     *   )
     * )
     *
     * @Route("/", name="default_index")
     *
     * @return JsonResponse
     */
    public function index()
    {
        return new JsonResponse();
    }
}
