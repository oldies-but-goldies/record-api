<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200907083104 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Creating tables';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql(<<<'SQL'
        create table customer
        (
            id    int auto_increment,
            email varchar(256) not null,
            constraint customer_pk
                primary key (id)
        );

        create table shopping_cart
            (
                id          int auto_increment,
            id_customer int                     not null,
            type        enum ('album','record') not null,
            reference   int                     not null,
            primary key (id),
            constraint shopping_cart_customer_id_fk
                foreign key (id_customer) references customer (id),
            unique index shopping_cart_uindex (id_customer, type, reference)
        );
        
        create table artist
            (
                id   int auto_increment,
            name varchar(256) not null,
            constraint artist_pk
                primary key (id)
        );
        
        create table record
            (
                id       int auto_increment,
            title    varchar(256) not null,
            duration int          not null,
            constraint record_pk
                primary key (id)
        );
        
        create table album
            (
                id          int auto_increment,
            title       varchar(256) not null,
            description text,
            constraint album_pk
                primary key (id)
        );
        
        create table album_records
            (
                id_album  int not null,
            id_record int not null,
            primary key (id_album, id_record),
            constraint album_records_album_id_fk
                foreign key (id_album) references album (id),
            constraint album_records_record_id_fk
                foreign key (id_record) references record (id)
        );
        
        create table artist_albums
            (
                id_artist int not null,
            id_album  int not null,
            primary key (id_artist, id_album),
            constraint artist_albums_artist_id_fk
                foreign key (id_artist) references artist (id),
            constraint artist_albums_album_id_fk
                foreign key (id_album) references album (id)
        );
        
        create table artist_records
            (
                id_artist int not null,
            id_record int not null,
            primary key (id_artist, id_record),
            constraint artist_records_artist_id_fk
                foreign key (id_artist) references artist (id),
            constraint artist_records_record_id_fk
                foreign key (id_record) references record (id)
        );
        SQL);
    }

    public function down(Schema $schema) : void
    {
        $this->addSql("drop table if exists artist_albums, artist_records, album_records, shopping_cart, artist, album, customer, record cascade");
    }
}
