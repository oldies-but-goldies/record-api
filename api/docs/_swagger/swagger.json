{
    "openapi": "3.0.0",
    "info": {
        "title": "Record Rest API",
        "description": "The vintage record shop 'Oldies but Goldies Berlin' needs a new website. They want to have all their record collection available for their customers to be consulted online.\\n\\nCustomers will be able to see the list of records and search by several fields. The record list will be maintained by the shop administrators.",
        "contact": {
            "name": "Alex",
            "email": "alex@webz.asia"
        },
        "license": {
            "name": "Apache 2.0",
            "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
        },
        "version": "1.0.0"
    },
    "paths": {
        "/record/{id}": {
            "get": {
                "tags": [
                    "Record"
                ],
                "operationId": "App\\Controller\\RecordController::getAction",
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "description": "ID value",
                        "required": true,
                        "schema": {
                            "type": "number"
                        }
                    }
                ],
                "responses": {
                    "default": {
                        "description": "successful operation",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/RecordApiResponse"
                                }
                            }
                        }
                    }
                }
            },
            "put": {
                "tags": [
                    "Record"
                ],
                "operationId": "App\\Controller\\RecordController::putAction",
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "description": "ID value",
                        "required": true,
                        "schema": {
                            "type": "number"
                        }
                    }
                ],
                "requestBody": {
                    "description": "Record data for full update",
                    "required": true,
                    "content": {
                        "application/json": {
                            "schema": {
                                "$ref": "#/components/schemas/DTORecord"
                            }
                        }
                    }
                },
                "responses": {
                    "200": {
                        "description": "successful operation",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/RecordApiResponse"
                                }
                            }
                        }
                    }
                }
            },
            "delete": {
                "tags": [
                    "Record"
                ],
                "operationId": "App\\Controller\\RecordController::deleteAction",
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "description": "ID value",
                        "required": true,
                        "schema": {
                            "type": "number"
                        }
                    }
                ],
                "responses": {
                    "default": {
                        "description": "successful operation",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/RecordApiResponse"
                                }
                            }
                        }
                    }
                }
            },
            "patch": {
                "tags": [
                    "Record"
                ],
                "operationId": "App\\Controller\\RecordController::patchAction",
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "description": "ID value",
                        "required": true,
                        "schema": {
                            "type": "number"
                        }
                    }
                ],
                "requestBody": {
                    "description": "Record data for partial update",
                    "required": true,
                    "content": {
                        "application/json": {
                            "schema": {
                                "$ref": "#/components/schemas/DTORecord"
                            }
                        }
                    }
                },
                "responses": {
                    "200": {
                        "description": "successful operation",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/RecordApiResponse"
                                }
                            }
                        }
                    }
                }
            }
        },
        "/record": {
            "post": {
                "tags": [
                    "Record"
                ],
                "operationId": "App\\Controller\\RecordController::postAction",
                "requestBody": {
                    "description": "Record data to create",
                    "required": true,
                    "content": {
                        "application/json": {
                            "schema": {
                                "$ref": "#/components/schemas/DTORecord"
                            }
                        }
                    }
                },
                "responses": {
                    "201": {
                        "description": "successful operation",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/RecordApiResponse"
                                }
                            }
                        }
                    }
                }
            }
        },
        "/record/search": {
            "post": {
                "tags": [
                    "Record"
                ],
                "operationId": "App\\Controller\\RecordController::searchAction",
                "requestBody": {
                    "description": "Record data to search",
                    "required": true,
                    "content": {
                        "application/json": {
                            "schema": {
                                "$ref": "#/components/schemas/DTOSearchRecord"
                            }
                        }
                    }
                },
                "responses": {
                    "200": {
                        "description": "successful operation",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/SearchRecordApiResponse"
                                }
                            }
                        }
                    }
                }
            }
        }
    },
    "components": {
        "schemas": {
            "ApiResponse": {
                "properties": {
                    "code": {
                        "title": "Code",
                        "description": "Code",
                        "type": "integer",
                        "format": "int32",
                        "nullable": true
                    },
                    "message": {
                        "title": "Message",
                        "description": "Message",
                        "type": "string",
                        "format": "string",
                        "nullable": true
                    }
                },
                "type": "object"
            },
            "DTOAlbum": {
                "properties": {
                    "id": {
                        "title": "ID",
                        "description": "DTO Album id",
                        "type": "integer",
                        "format": "int64"
                    },
                    "title": {
                        "title": "Title",
                        "description": "DTO Album title",
                        "type": "string",
                        "format": "string"
                    },
                    "description": {
                        "title": "Description",
                        "description": "DTO Album description",
                        "type": "string",
                        "format": "string"
                    }
                },
                "type": "object"
            },
            "DTOArtist": {
                "properties": {
                    "id": {
                        "title": "ID",
                        "description": "DTO Artist id",
                        "type": "integer",
                        "format": "int64"
                    },
                    "name": {
                        "title": "Name",
                        "description": "DTO Artist name",
                        "type": "string",
                        "format": "string"
                    }
                },
                "type": "object"
            },
            "DTORecord": {
                "properties": {
                    "id": {
                        "title": "ID",
                        "description": "DTO Record id",
                        "type": "integer",
                        "format": "int64"
                    },
                    "title": {
                        "title": "Title",
                        "description": "DTO Record title",
                        "type": "string",
                        "format": "string"
                    },
                    "duration": {
                        "title": "Duration",
                        "description": "DTO Record duration",
                        "type": "integer",
                        "format": "int32"
                    },
                    "albums": {
                        "title": "Albums",
                        "description": "Albums"
                    },
                    "artists": {
                        "title": "Artists",
                        "description": "Artists"
                    }
                },
                "type": "object"
            },
            "DTOSearchRecord": {
                "properties": {
                    "title": {
                        "title": "Title",
                        "description": "DTO Record title",
                        "type": "string",
                        "format": "string",
                        "nullable": true
                    },
                    "duration": {
                        "title": "Duration",
                        "description": "DTO Record duration",
                        "type": "integer",
                        "format": "int32",
                        "nullable": true
                    }
                },
                "type": "object"
            },
            "RecordApiResponse": {
                "properties": {
                    "code": {
                        "title": "Code",
                        "description": "Code",
                        "type": "integer",
                        "format": "int32",
                        "nullable": true
                    },
                    "message": {
                        "title": "Message",
                        "description": "Message",
                        "type": "string",
                        "format": "string",
                        "nullable": true
                    },
                    "data": {
                        "title": "Data",
                        "description": "Data",
                        "nullable": true,
                        "oneOf": [
                            {
                                "$ref": "#/components/schemas/DTORecord"
                            }
                        ]
                    }
                },
                "type": "object"
            },
            "SearchRecordApiResponse": {
                "properties": {
                    "code": {
                        "title": "Code",
                        "description": "Code",
                        "type": "integer",
                        "format": "int32",
                        "nullable": true
                    },
                    "message": {
                        "title": "Message",
                        "description": "Message",
                        "type": "string",
                        "format": "string",
                        "nullable": true
                    },
                    "data": {
                        "title": "Data",
                        "description": "Data"
                    }
                },
                "type": "object"
            }
        }
    }
}